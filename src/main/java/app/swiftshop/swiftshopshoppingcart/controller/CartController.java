package app.swiftshop.swiftshopshoppingcart.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import app.swiftshop.shoppingcart.datatypes.Cart;
import app.swiftshop.shoppingcart.datatypes.ErrorResponse;
import app.swiftshop.shoppingcart.datatypes.UpdateCartRequest;
import app.swiftshop.swiftshopshoppingcart.service.CartService;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Timed
public class CartController {

	@Autowired
	private CartService cartService;

	 @ApiOperation(value = "create new cart", nickname = "getCart", notes = "", response = Cart.class, tags={ "Cart", })
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "got cart", response = Cart.class),
	        @ApiResponse(code = 400, message = "bad request", response = ErrorResponse.class),
	        @ApiResponse(code = 404, message = "no cart found", response = ErrorResponse.class),
	        @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	    @RequestMapping(value = "/cart/{cartid}",
	        produces = { "application/json" }, 
	        consumes = { "application/json" },
	        method = RequestMethod.GET)
	public ResponseEntity<Cart> getCart(@ApiParam(value = "cart id",required=true) @PathVariable("cartid") String cartid,@ApiParam(value = "trace id" ,required=true) @RequestHeader(value="x-traceid", required=true) String xTraceid) {
		log.info("getCart cartid {} , xTraceId {}",cartid,xTraceid);
		Cart cart = cartService.getCart(cartid, xTraceid);
		return ResponseEntity.ok(cart);
	}
	
	 @ApiOperation(value = "update the cart", nickname = "updateCart", notes = "", response = Cart.class, tags={ "Cart", })
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "cart updated", response = Cart.class),
	        @ApiResponse(code = 400, message = "bad request", response = ErrorResponse.class),
	        @ApiResponse(code = 404, message = "no cart found", response = ErrorResponse.class),
	        @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	    @RequestMapping(value = "/cart/{cartid}",
	        produces = { "application/json" }, 
	        consumes = { "application/json" },
	        method = RequestMethod.PUT)
	public ResponseEntity<Cart> updateCart(String cartid, String xTraceid, @Valid UpdateCartRequest updateCartRequest) {
		
		Cart updatedCart= cartService.updateCart(cartid, xTraceid, updateCartRequest);
	    return ResponseEntity.ok(updatedCart);
	}
	
	
	 @ApiOperation(value = "create new cart", nickname = "createCart", notes = "", response = Cart.class, tags={ "Cart", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "new cart created", response = Cart.class),
        @ApiResponse(code = 400, message = "bad request", response = ErrorResponse.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
    @RequestMapping(value = "/cart/customer/{customerid}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
	public ResponseEntity<Cart> createCart(@ApiParam(value = "trace id" ,required=true) @RequestHeader(value="x-traceid", required=true) String xTraceid,@ApiParam(value = "customer id",required=true) @PathVariable("customerid") String customerid,@ApiParam(value = ""  )  @Valid @RequestBody UpdateCartRequest updateCartRequest) {
		Cart createdCart=cartService.createCart(xTraceid, customerid, updateCartRequest);
		return ResponseEntity.status(HttpStatus.CREATED).body(createdCart);
	}
	

}
