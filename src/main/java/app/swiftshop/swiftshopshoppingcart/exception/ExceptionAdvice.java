package app.swiftshop.swiftshopshoppingcart.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import app.swiftshop.shoppingcart.datatypes.ErrorResponse;

@ControllerAdvice
public class ExceptionAdvice {


    @ExceptionHandler(InvalidRequestException.class)
    private ResponseEntity<ErrorResponse>  badRequesthandler(InvalidRequestException exception)
    {
        ErrorResponse error=new ErrorResponse();
        error.setCode(exception.getCode());
        error.setMessage(exception.getMessage());

        return ResponseEntity.badRequest().body(error);

    }

    @ExceptionHandler(NotFoundException.class)
    private ResponseEntity<ErrorResponse>  badRequesthandler(NotFoundException exception)
    {
        ErrorResponse error=new ErrorResponse();
        error.setCode(exception.getCode());
        error.setMessage(exception.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);

    }
}
