package app.swiftshop.swiftshopshoppingcart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"app.swiftshop"})
public class SwiftshopShoppingcartApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwiftshopShoppingcartApplication.class, args);
	}

}
