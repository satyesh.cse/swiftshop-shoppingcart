package app.swiftshop.swiftshopshoppingcart.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages="app.swiftshop.swiftshopshoppingcart.repository")
public class RepositoryConfig {

}
