package app.swiftshop.swiftshopshoppingcart.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Slf4j
@Configuration
@EnableAspectJAutoProxy
public class LogAspect {

	@Around("execution(* app.swiftshop.swiftshopshoppingcart.controller.*.*(..))")
	private Object log(ProceedingJoinPoint jp) throws Throwable
	{
		log.debug("Enter method {} on class {}",jp.getSignature(),jp.getClass());
		Object o=jp.proceed();
		log.debug("Exit method {} on class {}",jp.getSignature(),jp.getClass());
		
		return o;
	}
}
