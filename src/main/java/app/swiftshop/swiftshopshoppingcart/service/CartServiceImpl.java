package app.swiftshop.swiftshopshoppingcart.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import app.swiftshop.shoppingcart.datatypes.Cart;
import app.swiftshop.shoppingcart.datatypes.Items;
import app.swiftshop.shoppingcart.datatypes.UpdateCartRequest;
import app.swiftshop.swiftshopshoppingcart.entity.CartItems;
import app.swiftshop.swiftshopshoppingcart.exception.InvalidRequestException;
import app.swiftshop.swiftshopshoppingcart.exception.NotFoundException;
import app.swiftshop.swiftshopshoppingcart.repository.CartRepository;

@Component

public class CartServiceImpl implements CartService {

    Logger log = LoggerFactory.getLogger(CartServiceImpl.class);

    @Autowired
    CartRepository cartRepo;

    @Override
    public Cart createCart(String xTraceid, String customerid,UpdateCartRequest updateCartRequest) {

        app.swiftshop.swiftshopshoppingcart.entity.Cart c=cartRepo.findByCustomerId(customerid);

        if(c!=null)
        {
            InvalidRequestException ire=new InvalidRequestException();
            ire.setCode("ERR-CART-01");
            ire.setMessage(String.format("customer already has an active cartId %s",c.getCartId()));
            log.error("cart already exist",ire);
            throw  ire;
        }
        app.swiftshop.swiftshopshoppingcart.entity.Cart cart=new app.swiftshop.swiftshopshoppingcart.entity.Cart();
        cart.setCustomerId(customerid);

        final String cartId=UUID.randomUUID().toString();
        log.debug("customerid {} -> cartId {}",customerid,cartId);

        cart.setCartId(cartId);
        cart.setCreateddate(LocalDate.now());
        cart.setCartItems(updateCartRequest.getItems().stream().map(p->this.maptoCartItems(p,cartId )).collect(Collectors.toList()));
        
        cartRepo.save(cart);
        return  mapToResponseCart(cart);
    }

    @Override
    public Cart getCart(String cartid, String xTraceid) {

        app.swiftshop.swiftshopshoppingcart.entity.Cart c=cartRepo.findByCartId(cartid);

        if(c==null)
        {
            NotFoundException ire=new NotFoundException();
            ire.setCode("ERR-CART-02");
            ire.setMessage(String.format("No cartid %s present",cartid));
            log.error("no cartId",ire);
            throw  ire;
        }

        return  mapToResponseCart(c);
    }

    @Override
    public Cart getCartByCustomer(String customerid, String xTraceid) {


        app.swiftshop.swiftshopshoppingcart.entity.Cart c=cartRepo.findByCustomerId(customerid);

        if(c==null)
        {
            NotFoundException ire=new NotFoundException();
            ire.setCode("ERR-CART-03");
            ire.setMessage(String.format("No cart present for customer %s ",customerid));
            log.error("no cartId",ire);
            throw  ire;
        }

        return  mapToResponseCart(c);

    }

    private Cart mapToResponseCart(app.swiftshop.swiftshopshoppingcart.entity.Cart cartEntity)
    {
        Cart cart=new Cart();
        cart.setCustomerid(cartEntity.getCustomerId());
        cart.setCartstatus(cartEntity.getStatus());
        cart.setCartvalue((float)cartEntity.getTotalValue());
        cart.setId(cartEntity.getCartId());
        if(cartEntity.getCartItems()!=null && !cartEntity.getCartItems().isEmpty())
        {
            cart.setItems(cartEntity.getCartItems().stream().map(this::mapItem).collect(Collectors.toList()));
        }
        return cart;
    }
    private Items mapItem(CartItems cartItem)
    {
        Items item=new Items();
        item.setItemid(cartItem.getItemId());
        item.setItemname(cartItem.getItemName());
        item.setQuantity(cartItem.getQuantity());
        item.setUnitprice(cartItem.getUnitPrice());
        return item;
    }
    private CartItems maptoCartItems(Items items,String cartId)
    {
    	CartItems cartitems=new CartItems();
    	cartitems.setItemId(items.getItemid());
    	cartitems.setCartId(cartId);
    	cartitems.setItemName(items.getItemname());
    	cartitems.setQuantity(items.getQuantity());
    	cartitems.setUnitPrice(items.getUnitprice());
        return cartitems;
    }

	@Override
	public Cart updateCart(String cartid, String xTraceid, UpdateCartRequest updateCartRequest) {
		app.swiftshop.swiftshopshoppingcart.entity.Cart orignialCart=cartRepo.findByCartId(cartid);

        if(orignialCart==null)
        {
            NotFoundException ire=new NotFoundException();
            ire.setCode("ERR-CART-03");
            ire.setMessage(String.format("No cart present for cartId %s ",cartid));
            log.error("no cartId",ire);
            throw  ire;
        }
        
        List<CartItems> orginalitems = orignialCart.getCartItems()==null?new ArrayList<>():orignialCart.getCartItems();
        
        List<CartItems> newItems=  updateCartRequest.getItems().stream().map(p->this.maptoCartItems(p, cartid)).collect(Collectors.toList());
        orginalitems.addAll(newItems);
        orignialCart.setCartItems(orginalitems);
        cartRepo.save(orignialCart);
        
        
		return mapToResponseCart(orignialCart);
	}
}
