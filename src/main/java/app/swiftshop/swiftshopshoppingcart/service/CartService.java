package app.swiftshop.swiftshopshoppingcart.service;

import app.swiftshop.shoppingcart.datatypes.Cart;
import app.swiftshop.shoppingcart.datatypes.UpdateCartRequest;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

public interface CartService {

   Cart createCart(String xTraceid, String customerid,UpdateCartRequest updateCartRequest);

   Cart getCart(String cartid, String xTraceid);

   Cart getCartByCustomer(String customerid, String xTraceid);
   
   Cart updateCart(String cartid, String xTraceid,UpdateCartRequest updateCartRequest);
   
   
   

}
