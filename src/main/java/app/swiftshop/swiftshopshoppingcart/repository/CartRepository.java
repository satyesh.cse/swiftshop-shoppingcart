package app.swiftshop.swiftshopshoppingcart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import app.swiftshop.swiftshopshoppingcart.entity.Cart;

public interface CartRepository extends JpaRepository<Cart, String> {

public Cart findByCustomerId(String customerId);
public Cart findByCartId(String cartId);

}
